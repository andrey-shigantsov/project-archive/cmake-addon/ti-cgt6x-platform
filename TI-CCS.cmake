if (CMAKE_TI_CSS_TOOLS)
  return()
endif()
set(CMAKE_TI_CSS_TOOLS 1)

set(CMAKE_TI_CSS_TOOLS_DEBUG 0 CACHE BOOL "")

set(TI_CCS_ROOT $ENV{TI_CCS_ROOT} CACHE PATH "" FORCE)

function(target_create_ti_ccs_project TARGET)

  set(PROJECT_NAME ${TARGET})
  set(CSS_BUILD_TARGET TI_CCS_build_${TARGET})

  set(options )
  set(oneValueArgs WS_DIR TI_CCS_DEVICE TI_TARGET TI_PLATFORM)
  set(multiValueArgs INCLUDES LINK_RESOURCES)
  cmake_parse_arguments(PARSE_ARGV 1 ARG "${options}" "${oneValueArgs}" "${multiValueArgs}")

  #message(STATUS "TI CCS Workspace: \"${ARG_WS_DIR}\"")

  set(app_css_includes "")
  get_property(includes TARGET ${TARGET} PROPERTY INCLUDE_DIRECTORIES)
  foreach(include_dir IN LISTS includes ARG_INCLUDES)
    set(app_css_includes "${app_css_includes} --include_path=\"${include_dir}\"")
  endforeach()

  set(app_css_link_resources )
  set(lr_mode "none")
  set(lr_dir )
  set(lr_virtual )
  set(lr_config )
  set(lr_reset_after_append_resource 0)
  foreach(resource IN LISTS ARG_LINK_RESOURCES)
    if (${lr_mode} STREQUAL "none")
      if (${resource} STREQUAL "ROOT_DIR")
        set(lr_dir )
        set(lr_virtual )
        set(lr_reset_after_append_resource 0)
        set(lr_mode "none")
      elseif(${resource} STREQUAL "VIRTUAL_DIRS")
        set(lr_virtual "@virtual")
        set(lr_reset_after_append_resource 0)
        set(lr_mode "dir")
      else()
        set(app_css_link_resources ${app_css_link_resources} -ccs.linkFile ${resource} ${lr_dir} ${lr_virtual} ${lr_config})
        if (lr_reset_after_append_resource)
          set(lr_dir )
          set(lr_virtual )
          set(lr_config )
          set(lr_reset_after_append_resource 0)
        endif()
      endif()
    elseif(${lr_mode} STREQUAL "dir")
      set(lr_dir "@dir" ${resource})
      set(lr_mode "none")
    endif()
  endforeach()
  unset(lr_mode)

  set(app_css_args
    -ccs.name ${PROJECT_NAME}
    -ccs.configurations ${CMAKE_BUILD_TYPE}
    -ccs.outputType executable

    -ccs.toolChain TI
    -ccs.autoGenerateMakefiles false
    -ccs.buildLocation "${CMAKE_CURRENT_BINARY_DIR}"
    -ccs.buildTarget "${CSS_BUILD_TARGET}"

    -ccs.device ${ARG_TI_CCS_DEVICE}
    -ccs.cmd none
    -ccs.products "com.ti.rtsc.SYSBIOS"

    -rtsc.enableDspBios
    -rtsc.target ${ARG_TI_TARGET}
    -rtsc.platform ${ARG_TI_PLATFORM}

    -ccs.setLinkerOptions "-o ${TARGET}"
    -ccs.setCompilerOptions ${app_css_includes}

    ${app_css_link_resources}

    -ccs.overwrite full)

  set(eclipse_args
    -noSplash
    -data "${ARG_WS_DIR}"
    -application com.ti.ccstudio.apps.projectCreate ${app_css_args})
  if (WIN32)
    set(css_eclipse_command ${TI_CCS_ROOT}/eclipse/eclipsec)
  else()
    set(css_eclipse_command ${TI_CCS_ROOT}/eclipse/eclipse)
  endif()
  set(gen_cmd "${css_eclipse_command} ${eclipse_args}")

  set(needGenerateCcsProject 0)
  if (NOT EXISTS ${ARG_WS_DIR}/${PROJECT_NAME})
    set(needGenerateCcsProject 1)
  elseif (NOT "${gen_cmd}" STREQUAL "${CMAKE_${CSS_BUILD_TARGET}_GENERATE_COMMAND}")
    set(needGenerateCcsProject 1)
  endif()

  if (needGenerateCcsProject)
    execute_process(COMMAND
      ${css_eclipse_command} ${eclipse_args}
      OUTPUT_VARIABLE eclipse_stdout
      RESULT_VARIABLE eclipse_result)
    set(CMAKE_${CSS_BUILD_TARGET}_GENERATE_COMMAND ${gen_cmd} CACHE INTERNAL "" FORCE)
    if (CMAKE_TI_CSS_TOOLS_DEBUG)
      message("gen cmd: ${CMAKE_${CSS_BUILD_TARGET}_GENERATE_COMMAND}")
    endif()
  endif()

  if (CMAKE_TI_CSS_TOOLS_DEBUG)
    #message(STATUS "CCS project generator eclipse params: ${eclipse_args}")
    message(STATUS "CCS project generator params: ${app_css_args}")
  endif()
  message(STATUS "${eclipse_stdout}")
  if (NOT ${eclipse_result} EQUAL 0)
    message(WARNING "Create CCS project failure: result ${eclipse_result}")
  else()
    set(out_file ${ARG_WS_DIR}/${PROJECT_NAME}/${CMAKE_BUILD_TYPE}/${TARGET})
    add_custom_target(${CSS_BUILD_TARGET}
      ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_BINARY_DIR}/${TARGET}" "${out_file}")
    add_dependencies(${CSS_BUILD_TARGET} ${TARGET})
  endif()
  #message(FATAL_ERROR "STOP POINT FOR DEBUG")

endfunction()
