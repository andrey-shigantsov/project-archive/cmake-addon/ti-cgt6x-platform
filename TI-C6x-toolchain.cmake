if(__TI_CGT6x_TOOLCHAIN_INCLUDED)
  return()
endif()
set(__TI_CGT6x_TOOLCHAIN_INCLUDED 1)

message("Startint to process ${CMAKE_CURRENT_LIST_FILE}")
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/Modules")
set(TI_CGT_C6x_ROOT $ENV{TI_CGT_C6x_ROOT} CACHE PATH "" FORCE)

set(CMAKE_SYSTEM_NAME TI-cgt6x)

macro(add_ti_compiler lang)
  set(CMAKE_${lang}_COMPILER "${TI_CGT_C6x_ROOT}/bin/cl6x")
  set(CMAKE_${lang}_COMPILER_ID_RUN TRUE)
  set(CMAKE_${lang}_COMPILER_ID TI-cgt6x)
  set(CMAKE_${lang}_COMPILER_FORCED TRUE)
  # if you encounter problems while cmake does compiler tests add the following
  set(CMAKE_${lang}_COMPILER_WORKS 1 CACHE INTERNAL "" FORCE)
  #skip ABI checks
  set(CMAKE_${lang}_ABI_COMPILED 1 CACHE INTERNAL "" FORCE)
  #disable ranlib
  unset(CMAKE_${lang}_ARCHIVE_FINISH)
endmacro()
add_ti_compiler(C)
add_ti_compiler(CXX)
add_ti_compiler(ASM)

set(CMAKE_PREFIX_PATH ${TI_CGT_C6x_ROOT})
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
