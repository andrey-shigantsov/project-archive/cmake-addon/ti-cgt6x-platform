if(__TI_CGT6x_PLATFORM_INCLUDED)
  return()
endif()
set(__TI_CGT6x_PLATFORM_INCLUDED 1)

message("Startint to process ${CMAKE_CURRENT_LIST_FILE}")

if (NOT TI_CGT_C6x_ROOT)
  message(FATAL_ERROR "TI_CGT_C6x_ROOT variable not set")
endif()

set_property(GLOBAL PROPERTY TARGET_SUPPORTS_SHARED_LIBS FALSE)

set(CMAKE_SYSTEM_INCLUDE_PATH ${TI_CGT_C6x_ROOT}/include )
set(CMAKE_SYSTEM_LIBRARY_PATH ${TI_CGT_C6x_ROOT}/lib )
set(CMAKE_SYSTEM_PROGRAM_PATH ${TI_CGT_C6x_ROOT}/bin )

set(CMAKE_AR "${CMAKE_SYSTEM_PROGRAM_PATH}/ar6x")
set(CMAKE_STRIP "${CMAKE_SYSTEM_PROGRAM_PATH}/strip6x")
set(CMAKE_LINKER ${CMAKE_C_COMPILER})

set(CMAKE_EXE_LINKER_FLAGS_INIT "--reread_libs --warn_sections")
set(CMAKE_EXE_LINKER_FLAGS_DEBUG_INIT )
set(CMAKE_EXE_LINKER_FLAGS_RELEASE_INIT "--unused_section_elimination=on")

macro(init_ti_cgt6x_compiler lang)

  set(CMAKE_${lang}_FLAGS_INIT "--diag_warning=225 --diag_wrap=off --display_error_number --gen_func_subsections=on --gen_data_subsections=on")
  set(CMAKE_${lang}_FLAGS_DEBUG_INIT "-g --opt_level=off")
  set(CMAKE_${lang}_FLAGS_RELEASE_INIT "--opt_level=3")

endmacro()

init_ti_cgt6x_compiler(C)
init_ti_cgt6x_compiler(CXX)
init_ti_cgt6x_compiler(ASM)

include_directories("${TI_CGT_C6x_ROOT}/include")
